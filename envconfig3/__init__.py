# -*- coding: utf-8 -*-
import ast
from collections import UserDict
import os


def default_decoder(key: str, value: str):
    try:
        return ast.literal_eval(value)
    except (ValueError, SyntaxError):
        return value


class EnvConfig(UserDict):
    decoder = default_decoder

    def __init__(self, prefix: str ='', decoder=None):
        self.decoder = decoder or self.decoder
        if prefix:
            prefix = '{0}_'.format(prefix).upper()
        self.data = {
            key[len(prefix):].lower(): value
            for key, value in os.environ.items()
            if key.startswith(prefix)
        }

    def __getattr__(self, item: str):
        try:
            return self[item]
        except KeyError as e:
            raise AttributeError(e)

    def __getitem__(self, key: str):
        key_lower = key.lower()
        value = super().__getitem__(key_lower)
        return self.decoder(value)
